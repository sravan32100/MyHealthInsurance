<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>  
<!DOCTYPE html>
<body>	
   
     	<form action="calculate" method="POST" id="needs-validation">
			<fieldset class="container">
				<div class="form-row">

					<div class="form-group col-md-12">
						<label for="inputEmail4" class="col-form-label"><strong>Name</strong></label> <input
							type="text" name="userName" class="form-control" id="inputEmail4"
							placeholder="Name" required> 
					</div>
					 

				</div>
				 <div class="form-row">
				
				<div class="form-group col-md-6">
						<label  class="col-form-label"><strong>Gender</strong></label><br />
						<div class="form-check form-check-inline" id="inlineRadio11">
							<label class="custom-control custom-radio"> 
							<input class="custom-control-input" type="radio" name="gender"
								id="inlineRadio1" value="male" required> 
								 <span class="custom-control-indicator"></span>
								  <span class="custom-control-description">Male</span>
							</label>
						</div>
						<div class="form-check form-check-inline">
							<label class="custom-control custom-radio"> 
							<input class="custom-control-input" type="radio" name="gender"
								id="inlineRadio2" value="female" required>
								 <span class="custom-control-indicator"></span>
								 <span class="custom-control-description"> Female </span>
							</label>
						</div>
						<div class="form-check form-check-inline">
							<label class="custom-control custom-radio"> 
							<input  class="custom-control-input" type="radio" name="gender"
								id="inlineRadio3" value="other" required>
								 <span class="custom-control-indicator"></span>
								  <span class="custom-control-description"> Other </span>
							</label>
						</div>
					</div>
				 <div class="form-group col-md-6">
						<label for="inputZip" class="col-form-label"><strong>Age</strong></label>
						 <input   type="number" min="1"  max="110" step="1" class="form-control" name="age" required>
					</div> 

				</div>
				 <div class="form-group">
					<label for="inputAddress" class="col-form-label"><strong>Current Health</strong></label>
					<br/>
					<div class="form-check form-check-inline" id="inputAddress">
					 	<label class="custom-control custom-checkbox">
						  <input type="checkbox" name="hypertension" class="custom-control-input">
						  <span class="custom-control-indicator"></span>
						  <span class="custom-control-description">Hypertension</span>
						</label>
						
							<label class="custom-control custom-checkbox">
						  <input type="checkbox"  name="bloodpressure"  class="custom-control-input">
						  <span class="custom-control-indicator"></span>
						  <span class="custom-control-description">Blood pressure</span>
						</label>
						
							<label class="custom-control custom-checkbox">
						  <input type="checkbox" name="bloodsugar" class="custom-control-input">
						  <span class="custom-control-indicator"></span>
						  <span class="custom-control-description">Blood sugar</span>
						</label>
						
						<label class="custom-control custom-checkbox">
						  <input type="checkbox" name="overweight" class="custom-control-input">
						  <span class="custom-control-indicator"></span>
						  <span class="custom-control-description">Overweight</span>
						</label>
					</div>
				</div>
				 <div class="form-group">
					<label for="inputAddress" class="col-form-label"><strong>Habits</strong></label>
					<br/>
					<div class="form-check form-check-inline" id="inputAddress">
					 	<label class="custom-control custom-checkbox">
						  <input type="checkbox" name="smoking" class="custom-control-input">
						  <span class="custom-control-indicator"></span>
						  <span class="custom-control-description">Smoking</span>
						</label>
						
							<label class="custom-control custom-checkbox">
						  <input type="checkbox"  name="alcohol"  class="custom-control-input">
						  <span class="custom-control-indicator"></span>
						  <span class="custom-control-description">Alcohol</span>
						</label>
						
							<label class="custom-control custom-checkbox">
						  <input type="checkbox" name="dailyexercise" class="custom-control-input">
						  <span class="custom-control-indicator"></span>
						  <span class="custom-control-description">Daily exercise</span>
						</label>
						
						<label class="custom-control custom-checkbox">
						  <input type="checkbox" name="drugs" class="custom-control-input">
						  <span class="custom-control-indicator"></span>
						  <span class="custom-control-description">Drugs</span>
						</label>
					</div>
				</div>
				 <button type="submit" class="btn btn-primary btn-lg">Submit</button> 
				 	</fieldset> 
		</form>
	</body>
</html>